import {
    addPokemonToCollection,
    isInCollection,
} from "./../../utils/localstorage";
import { Pokemon } from "./../../models/pokemon.model";
import { Component, Input } from "@angular/core";

@Component({
    selector: "pokemon-info",
    templateUrl: "./pokemon-info.component.html",
    styleUrls: ["./pokemon-info.component.scss"],
})
export class PokemonInfoComponent {
    @Input() pokemon = {} as Pokemon;

    collectPokemon(): void {
        addPokemonToCollection(this.pokemon.id);
    }

    canCollect(): boolean {
        return isInCollection(this.pokemon.id);
    }
}
