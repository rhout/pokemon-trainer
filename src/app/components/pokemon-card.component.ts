import { Component, EventEmitter, Input, Output } from "@angular/core";
import { Pokemon } from "../models/pokemon.model";

@Component({
    selector: "pokemon-card",
    template: `<mat-card class="card" (click)="onPokemonClick(pokemon)">
        <mat-card-title class="title">{{ pokemon.name }}</mat-card-title>
        <img mat-card-image [src]="pokemon.image" />
    </mat-card>`,
    styleUrls: ["./pokemon-card.component.scss"],
})
export class PokemonCardComponent {
    @Input() pokemon = {} as Pokemon;
    @Output() cardClicked: EventEmitter<number> = new EventEmitter();

    onPokemonClick(pokemon: Pokemon): void {
        this.cardClicked.emit(pokemon.id);
    }
}
