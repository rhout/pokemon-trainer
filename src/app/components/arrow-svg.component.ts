import { Component, EventEmitter, Output } from "@angular/core";

@Component({
    selector: "arrow-icon",
    template: `<div (click)="onArrowClick()">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
            <polyline
                points="14 19 21 12 14 5"
                fill="none"
                stroke="#3b4cca"
                stroke-miterlimit="10"
            />
            <line
                x1="21"
                y1="12"
                x2="2"
                y2="12"
                fill="none"
                stroke="#3b4cca"
                stroke-miterlimit="10"
            />
        </svg>
    </div>`,

    styleUrls: ["./arrow-svg.component.scss"],
})
export class ArrowIconComponent {
    @Output() arrowClick: EventEmitter<void> = new EventEmitter();

    onArrowClick(): void {
        this.arrowClick.emit();
    }
}
