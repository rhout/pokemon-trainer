import { PokemonService } from "./../../services/pokemon.service";
import { Router } from "@angular/router";
import { Component } from "@angular/core";
import { deleteCollection, deleteUser } from "../../utils/localstorage";

@Component({
    selector: "navbar",
    templateUrl: "./navbar.component.html",
    styleUrls: ["./navbar.component.scss"],
})
export class NavbarComponent {
    constructor(
        private router: Router,
        private pokemonService: PokemonService
    ) {}
    goToDashboard(): void {
        this.router.navigateByUrl("/dashboard");
    }

    goToCatalogue(): void {
        this.router.navigateByUrl("/catalogue");
    }

    logOut(): void {
        deleteUser();
        deleteCollection();
        this.pokemonService.clearPokemonCollection();
        this.router.navigateByUrl("/get-started");
    }
}
