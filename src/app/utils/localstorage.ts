const COLLECTION = "collection";
const NAME = "name";

export const getUser = (): string => {
    return localStorage.getItem(NAME) || "";
};

export const setUser = (name: string): void => {
    localStorage.setItem(NAME, name);
};

export const deleteUser = (): void => {
    localStorage.removeItem(NAME);
};

export const addPokemonToCollection = (pokemonId: number): void => {
    const currentCollection = getColletionAsString();
    let addString = `${pokemonId}`;
    if (currentCollection) {
        addString = "," + pokemonId;
    }
    localStorage.setItem(COLLECTION, currentCollection + addString);
};

const getColletionAsString = (): string => {
    return localStorage.getItem(COLLECTION) || "";
};

export const isInCollection = (id: number): boolean => {
    return getPokemonIdCollection().includes(id);
};

export const getPokemonIdCollection = (): number[] => {
    const savedString = getColletionAsString();
    const stringIds = savedString.split(",");
    return stringIds[0] !== "" ? stringIds.map(Number) : [];
};

export const deleteCollection = (): void => {
    localStorage.removeItem(COLLECTION);
};
