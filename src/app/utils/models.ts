import { Stat } from "./../models/stats.model";

export const getStatsFromApi = (stats: any): Stat => {
    return {
        hp: stats[0].base_stat,
        attack: stats[1].base_stat,
        defense: stats[2].base_stat,
        specialAttack: stats[3].base_stat,
        specialDefense: stats[4].base_stat,
        speed: stats[5].base_stat,
    };
};

export const getAbilitiesFromApiObject = (abilities: any) => {
    const customAbilities: string[] = [];
    for (const ability of abilities) {
        customAbilities.push(ability.ability.name.replace("-", " "));
    }
    return customAbilities;
};

export const getMovesFromApiObject = (moves: any): string[] => {
    const customMoves: string[] = [];
    for (const move of moves) {
        customMoves.push(move.move.name.replace("-", " "));
    }
    return customMoves;
};

export const getTypesFromApiObject = (types: any): string[] => {
    const customTypes: string[] = [];
    for (const type of types) {
        customTypes.push(type.type.name.replace("-", " "));
    }
    return customTypes;
};
