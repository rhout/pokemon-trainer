export const isEmpty = (object: object) => {
    return Object.keys(object).length === 0 && object.constructor === Object;
};
