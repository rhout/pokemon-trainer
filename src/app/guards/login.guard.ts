import { PokemonService } from "./../services/pokemon.service";
import { Pokemon } from "./../models/pokemon.model";
import { Injectable } from "@angular/core";
import { CanActivate, UrlTree, Router } from "@angular/router";
import { Observable } from "rxjs";
import { getUser } from "../utils/localstorage";

@Injectable({
    providedIn: "root",
})
export class LoginGuard implements CanActivate {
    pokemon: Pokemon = {} as Pokemon;
    constructor(private router: Router) {}

    canActivate():
        | Observable<boolean | UrlTree>
        | Promise<boolean | UrlTree>
        | boolean
        | UrlTree {
        if (getUser()) {
            this.router.navigateByUrl("/dashboard");
            return false;
        }
        return true;
    }
}
