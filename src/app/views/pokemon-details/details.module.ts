import { DetailsComponent } from "./details.component";
import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";

const routes = [
    {
        path: "",
        component: DetailsComponent,
    },
];

@NgModule({
    declarations: [],
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers: [],
})
export class DetailsModule {}
