import { PokemonService } from "../../services/pokemon.service";
import { Pokemon } from "../../models/pokemon.model";
import { Component, OnInit } from "@angular/core";
import { isEmpty } from "../../utils/utils";
import { ActivatedRoute } from "@angular/router";

@Component({
    selector: "details-page",
    templateUrl: "./details.component.html",
    styleUrls: ["./details.component.scss"],
})
export class DetailsComponent implements OnInit {
    pokemon: Pokemon = {} as Pokemon;
    constructor(
        private route: ActivatedRoute,
        private pokemonService: PokemonService
    ) {}

    ngOnInit(): void {
        const pokemonId = this.route.snapshot.paramMap.get("pokemonId");
        let idAsNumber = -1;
        if (pokemonId) {
            idAsNumber = parseInt(pokemonId, 10);
        }
        this.pokemonService
            .getPokemonById(idAsNumber)
            .subscribe((pokemon: Pokemon) => {
                this.pokemon = pokemon;
            });
    }

    isNoPokemon(pokemon: Pokemon): boolean {
        return isEmpty(pokemon);
    }
}
