import { Router } from "@angular/router";
import { Component, OnInit } from "@angular/core";
import { PokemonService } from "../../services/pokemon.service";
import { Pokemon } from "../../models/pokemon.model";

@Component({
    selector: "catalogue",
    templateUrl: "./catalogue.component.html",
    styleUrls: ["./catalogue.component.scss"],
})
export class CatalogueComponent implements OnInit {
    pokemons: Pokemon[] = [];
    page = 0;

    constructor(
        private router: Router,
        private pokemonService: PokemonService
    ) {}

    ngOnInit(): void {
        this.fetchPokemons();
    }

    fetchPokemons(): void {
        this.pokemonService
            .getPokemonsForPage(this.page)
            .subscribe((pokemons: Pokemon[]) => {
                this.pokemons = pokemons;
            });
    }

    prevPage(): void {
        this.page--;
        this.fetchPokemons();
    }

    nextPage(): void {
        this.page++;
        this.fetchPokemons();
    }

    onPokemonClicked(pokemonId: number): void {
        this.router.navigateByUrl(`/details/${pokemonId}`);
    }
}
