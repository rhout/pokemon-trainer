import { RouterModule } from "@angular/router";
import { CatalogueComponent } from "./catalogue.component";
import { NgModule } from "@angular/core";

const routes = [
    {
        path: "",
        component: CatalogueComponent,
    },
];

@NgModule({
    declarations: [],
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers: [],
})
export class CatalogueModule {}
