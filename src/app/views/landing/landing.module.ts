import { LandingComponent } from "./landing.component";
import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";

const routes = [
    {
        path: "",
        component: LandingComponent,
    },
];

@NgModule({
    declarations: [],
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers: [],
})
export class LandingModule {}
