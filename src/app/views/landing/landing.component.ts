import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { setUser } from "src/app/utils/localstorage";

@Component({
    selector: "landing",
    templateUrl: "./landing.component.html",
    styleUrls: ["./landing.component.scss"],
})
export class LandingComponent {
    name = "";
    constructor(private router: Router) {}

    onGoClick(): void {
        setUser(this.name);
        this.router.navigateByUrl("/dashboard");
    }
}
