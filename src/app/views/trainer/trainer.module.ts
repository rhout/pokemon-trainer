import { TrainerComponent } from "./trainer.component";
import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";

const routes = [
    {
        path: "",
        component: TrainerComponent,
    },
];

@NgModule({
    declarations: [],
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers: [],
})
export class TrainerModule {}
