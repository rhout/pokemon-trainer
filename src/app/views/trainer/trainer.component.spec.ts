/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { By } from "@angular/platform-browser";
import { DebugElement } from "@angular/core";

import { trainerComponent } from "./trainer.component";

describe("trainerComponent", () => {
    let component: trainerComponent;
    let fixture: ComponentFixture<trainerComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [trainerComponent],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(trainerComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });
});
