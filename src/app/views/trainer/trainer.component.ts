import { Router } from "@angular/router";
import { Subscription } from "rxjs";
import { Pokemon } from "./../../models/pokemon.model";
import { PokemonService } from "./../../services/pokemon.service";
import { getPokemonIdCollection, getUser } from "./../../utils/localstorage";
import { Component, OnDestroy, OnInit } from "@angular/core";

@Component({
    selector: "trainer",
    templateUrl: "./trainer.component.html",
    styleUrls: ["./trainer.component.scss"],
})
export class TrainerComponent implements OnInit, OnDestroy {
    name = getUser();
    pokemons: Pokemon[] = [];
    private readonly pokemons$: Subscription;

    constructor(
        private router: Router,
        private pokemonService: PokemonService
    ) {
        this.pokemons$ = this.pokemonService.collectedPokemons$.subscribe(
            (pokemons: Pokemon[]) => {
                this.pokemons = pokemons;
            }
        );
    }

    ngOnInit(): void {
        const savedIds = getPokemonIdCollection();
        this.pokemonService.getCollectedPokemons(savedIds);
    }

    ngOnDestroy(): void {
        this.pokemons$.unsubscribe();
    }

    onPokemonClicked(pokemonId: number): void {
        this.router.navigateByUrl(`/details/${pokemonId}`);
    }
}
