import { LoginGuard } from "./guards/login.guard";
import { AuthGuard } from "./guards/auth.guard";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

const routes: Routes = [
    {
        path: "",
        loadChildren: () =>
            import(
                "./navbar-layouts/without-navbar/without-navbar.module"
            ).then((m) => m.WithoutNavbarModule),
        canActivate: [LoginGuard],
    },
    {
        path: "",
        canActivate: [AuthGuard],
        loadChildren: () =>
            import("./navbar-layouts/with-navbar/with-navbar.module").then(
                (m) => m.WithNavbarModule
            ),
    },
    { path: "**", pathMatch: "full", redirectTo: "/get-started" },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule {}
