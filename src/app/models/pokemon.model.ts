import { Stat } from "./stats.model";
export interface Pokemon {
    id: number;
    name: string;
    image: string;
    types?: Array<string>;
    base_stats?: Stat;
    base_experience?: number;
    height?: number;
    weight?: number;
    abilities?: Array<string>;
    moves?: Array<string>;
}
