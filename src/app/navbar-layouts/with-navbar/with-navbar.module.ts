import { MatButtonModule } from "@angular/material/button";
import { FormsModule } from "@angular/forms";
import { DetailsComponent } from "./../../views/pokemon-details/details.component";
import { PokemonInfoComponent } from "./../../components/pokemon-info/pokemon-info.component";
import { ArrowIconComponent } from "./../../components/arrow-svg.component";
import { CatalogueComponent } from "./../../views/catalogue/catalogue.component";
import { TrainerComponent } from "./../../views/trainer/trainer.component";
import { PokemonCardComponent } from "./../../components/pokemon-card.component";
import { MatCardModule } from "@angular/material/card";
import { MainLayoutComponent } from "./with-navbar.component";
import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

const routes = [
    {
        path: "",
        component: MainLayoutComponent,
        children: [
            {
                path: "dashboard",
                loadChildren: () =>
                    import("../../views/trainer/trainer.module").then(
                        (m) => m.TrainerModule
                    ),
            },
            {
                path: "catalogue",
                loadChildren: () =>
                    import("../../views/catalogue/catalogue.module").then(
                        (m) => m.CatalogueModule
                    ),
            },
            {
                path: "details/:pokemonId",
                loadChildren: () =>
                    import("../../views/pokemon-details/details.module").then(
                        (m) => m.DetailsModule
                    ),
            },
        ],
    },
];

@NgModule({
    declarations: [
        TrainerComponent,
        CatalogueComponent,
        PokemonCardComponent,
        ArrowIconComponent,
        PokemonInfoComponent,
        DetailsComponent,
    ],
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        MatCardModule,
        FormsModule,
        MatButtonModule,
    ],
    exports: [RouterModule],
    providers: [],
})
export class WithNavbarModule {}
