import { Component } from "@angular/core";

@Component({
    selector: "main-layout",
    template: `<navbar></navbar>
        <div class="body">
            <router-outlet></router-outlet>
        </div>`,
    styleUrls: ["./with-navbar.component.scss"],
})
export class MainLayoutComponent {}
