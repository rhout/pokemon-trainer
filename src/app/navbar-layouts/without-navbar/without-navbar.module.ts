import { LandingComponent } from "./../../views/landing/landing.component";
import { MatInputModule } from "@angular/material/input";
import { FormsModule } from "@angular/forms";
import { MatButtonModule } from "@angular/material/button";
import { LoginLayoutComponent } from "./without-navbar.component";
import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";

const routes = [
    {
        path: "",
        component: LoginLayoutComponent,
        children: [
            {
                path: "get-started",
                loadChildren: () =>
                    import("../../views/landing/landing.module").then(
                        (m) => m.LandingModule
                    ),
            },
        ],
    },
];

@NgModule({
    declarations: [LandingComponent],
    imports: [
        RouterModule.forChild(routes),
        MatInputModule,
        FormsModule,
        MatButtonModule,
    ],
    exports: [RouterModule],
    providers: [],
})
export class WithoutNavbarModule {}
