import {
    getAbilitiesFromApiObject,
    getMovesFromApiObject,
    getStatsFromApi,
    getTypesFromApiObject,
} from "./../utils/models";
import { Pokemon } from "../models/pokemon.model";
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";
import { map } from "rxjs/operators";

@Injectable({
    providedIn: "root",
})
export class PokemonService {
    public readonly collectedPokemons$: BehaviorSubject<
        Pokemon[]
    > = new BehaviorSubject<Pokemon[]>([]);

    private baseUrl = "https://pokeapi.co/api/v2/pokemon/";
    private urlOffset = "?offset=";

    constructor(private http: HttpClient) {}

    getPokemonById(id: number): Observable<Pokemon> {
        return this.http.get<Pokemon>(`${this.baseUrl}${id}`).pipe(
            map(
                (response: any): Pokemon => {
                    const {
                        name,
                        base_experience,
                        height,
                        weight,
                        stats,
                        abilities,
                        moves,
                        types,
                        sprites: { front_default: image },
                    } = response;

                    return {
                        id,
                        name: this.capitalize(name),
                        image,
                        abilities: getAbilitiesFromApiObject(abilities),
                        base_experience,
                        base_stats: getStatsFromApi(stats),
                        height,
                        moves: getMovesFromApiObject(moves),
                        types: getTypesFromApiObject(types),
                        weight,
                    };
                }
            )
        );
    }

    getPokemonsForPage(page: number): Observable<Pokemon[]> {
        return this.http
            .get<Pokemon[]>(`${this.baseUrl}${this.urlOffset}${page * 20}`)
            .pipe(
                map((response: any) =>
                    response.results.map((pokemon: any) => {
                        const splitUrl = pokemon.url.split("/");
                        const id = splitUrl[splitUrl.length - 2];
                        return {
                            id,
                            name: this.capitalize(pokemon.name),
                            image: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${id}.png`,
                        };
                    })
                )
            );
    }

    getCollectedPokemons(ids: number[]): void {
        for (const id of this.getUnsavedPokemons(ids)) {
            this.http
                .get<Pokemon>("https://pokeapi.co/api/v2/pokemon/" + id)
                .pipe(
                    map(
                        (response: any): Pokemon => {
                            const {
                                name,
                                sprites: { front_default: image },
                            } = response;
                            return {
                                id,
                                name: this.capitalize(name),
                                image,
                            };
                        }
                    )
                )
                .toPromise()
                .then((pokemon: Pokemon) => {
                    this.collectedPokemons$.next([
                        ...this.collectedPokemons$.value,
                        pokemon,
                    ]);
                });
        }
    }

    private capitalize(word: string): string {
        return word.replace(/^\w/, (character: string) =>
            character.toUpperCase()
        );
    }

    private getUnsavedPokemons(ids: number[]): number[] {
        const newIds = [];
        const storedPokemonIds = this.getIdsFromPokemonObserver();
        for (const id of ids) {
            if (!storedPokemonIds.includes(id)) {
                newIds.push(id);
            }
        }

        return newIds;
    }

    private getIdsFromPokemonObserver(): number[] {
        return this.collectedPokemons$.value.map((pokemon: Pokemon) => {
            const { image } = pokemon;
            const firstIndex = image.lastIndexOf("/");
            const lastIndex = image.lastIndexOf(".");
            return parseInt(image.substring(firstIndex + 1, lastIndex), 10);
        });
    }

    clearPokemonCollection(): void {
        this.collectedPokemons$.next([]);
    }
}
